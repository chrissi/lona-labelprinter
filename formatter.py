import pathlib
from fpdf import FPDF

HERE = pathlib.Path(__file__).resolve().parent


class Brother100x62_HOA(FPDF):
    NAME = "Brother 100x62 HOA"

    def __init__(self):
        super().__init__(orientation="l", format=(62, 100))
        self.add_font(family="Geo", fname=HERE / "contrib" / "Geo-Regular.ttf")
        self.add_font(family="Fira", fname=HERE / "contrib" / "FiraSans-Regular.ttf")
        self.set_margin(0)
        self.set_left_margin(2)
        self.set_auto_page_break(margin=0, auto=True)

    def header(self):
        self.image(HERE / "contrib/specht.png", x=100 - 25, y=62 - 44, w=20, h=50, keep_aspect_ratio=True)

    def add_text(self, text: str):
        self.add_page()

        self.set_font("Geo")
        self.set_font_size(35)
        self.set_char_spacing(-1)
        self.multi_cell(w=0, txt=text, align="L")


class Brother62x29_HOA(FPDF):
    NAME = "Brother 62x29 HOA"

    def __init__(self):
        super().__init__(orientation="l", format=(29, 62))
        self.add_font(family="Geo", fname=HERE / "contrib" / "Geo-Regular.ttf")
        self.add_font(family="Fira", fname=HERE / "contrib" / "FiraSans-Regular.ttf")
        self.set_margin(0)
        self.set_left_margin(2)
        self.set_top_margin(2)
        self.set_auto_page_break(margin=0, auto=True)

    def header(self):
        self.image(HERE / "contrib/specht.png", x=62 - 14, y=0, w=10, h=29, keep_aspect_ratio=True)

    def add_text(self, text: str):
        self.add_page()

        self.set_font("Geo")
        self.set_font_size(20)
        self.set_char_spacing(-1)
        self.multi_cell(w=0, txt=text, align="L")


class Brother100x62(FPDF):
    NAME = "Brother 100x62 blank"

    def __init__(self):
        super().__init__(orientation="l", format=(62, 100))
        self.set_margin(0)
        self.set_left_margin(2)
        self.set_top_margin(2.5)
        self.set_auto_page_break(margin=0, auto=True)

    def add_text(self, text: str):
        self.add_page()

        self.set_font("Arial")
        self.set_font_size(35)
        self.set_char_spacing(-1)
        self.multi_cell(w=0, txt=text, align="C")


class Brother62x29(FPDF):
    NAME = "Brother 62x29 blank"

    def __init__(self):
        super().__init__(orientation="l", format=(29, 62))
        self.add_font(family="Fira", fname=HERE / "contrib" / "FiraSans-Regular.ttf")
        self.set_margin(0)
        self.set_left_margin(2)
        self.set_top_margin(2.5)
        self.set_auto_page_break(margin=0, auto=True)

    def add_text(self, text: str):
        self.add_page()

        self.set_font("Arial")
        self.set_font_size(20)
        self.set_char_spacing(-1)
        self.multi_cell(w=0, txt=text, align="C")


class Zebra50x30(FPDF):
    NAME = "Zebra 50x30 mm die cut, direct print"
    HINT = "hostname\n.rest.of.fqdn.example.com"

    def __init__(self):
        super().__init__(orientation="p", format=(50, 30))
        self.set_margin(0)
        self.set_left_margin(2)
        self.set_top_margin(2.5)
        self.set_auto_page_break(margin=0, auto=True)

    def add_text(self, text: str):
        self.add_page()

        hostname, netname = (text.splitlines() + [""] * 2)[:2]

        self.set_font("Arial")
        self.set_xy(2, 10)
        self.set_font_size(18)
        self.set_char_spacing(-1)
        self.multi_cell(w=0, txt=hostname, align="L")

        self.set_xy(2, 18)
        self.set_font_size(9)
        self.set_char_spacing(-1)
        self.multi_cell(w=0, txt=netname, align="L")


formatter = {
    "Brother100x62_HOA": Brother100x62_HOA,
    "Brother62x29_HOA": Brother62x29_HOA,
    "Brother62x29": Brother62x29,
    "Brother100x62": Brother100x62,
    "Zebra50x30": Zebra50x30,
}

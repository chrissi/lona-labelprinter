from __future__ import annotations

import contextlib
import json
import pathlib
import subprocess
import tempfile
import os
import re
import threading
import time

import yaml
from urllib.parse import urlencode

import lona.unique_ids
from lona import App, View, AbstractNode
from lona.server import Request
from lona_picocss import install_picocss
from lona_picocss.html import HTML, H1, TextArea, Img, Button, Icon, Card, Div, Ul, Li, A
from lona.html import CLICK
import schema

from formatter import formatter


config_fn = os.environ.get("LABELPRINTER_CONFIG", "config.yaml")
with open(config_fn) as fh:
    config = yaml.safe_load(fh)

config_schema = schema.Schema(
    {
        "printers": [
            {
                "printer_name": str,
                "label": str,
                "formatter": [schema.And(str, lambda n: n in formatter.keys())],
                schema.Optional("lpr_extra_args"): str,
            }
        ]
    },
    ignore_extra_keys=True,
)
config_schema.validate(config)

app = App(__file__)
install_picocss(app, debug=True)

app.settings.MAX_RUNTIME_THREADS = 50
app.settings.MAX_WORKER_THREADS = 100
app.settings.MAX_STATIC_THREADS = 20

app.settings.PICOCSS_BRAND = "Lona Labelgenerator"
app.settings.PICOCSS_TITLE = "Lona Labelgenerator"
app.settings.PICOCSS_COLOR_SCHEME = "light-green"
app.settings.PICOCSS_HEADER = False

app.settings.SESSIONS = True
app.settings.SESSIONS_KEY_NAME = "lona-labelprinter-session"

ipc = dict()


class HistoryContainer:
    _instance = None
    _instance_lock = threading.Lock()
    _history_lock = threading.Lock()

    def __new__(cls):
        if cls._instance is None:
            with cls._instance_lock:
                if not cls._instance:
                    cls._instance = super().__new__(cls)
        return cls._instance

    def __init__(self):
        self.history = {}
        with contextlib.suppress(FileNotFoundError):
            with open("history.json") as fh:
                self.history = json.load(fh)

    def get_history(self, session):
        with self.__class__._instance_lock:
            if session in self.history:
                return self.history[session]
            else:
                return []

    def append_history(self, session, text):
        with self.__class__._instance_lock:
            if session in self.history:
                if text not in self.history[session]:
                    self.history[session].append(text)
                if len(self.history[session]) > 10:
                    self.history[session] = self.history[session][-10:]
            else:
                self.history[session] = [text]

            with open("history.json", "w") as fh:
                json.dump(self.history, fh)


@app.route("/genpng", interactive=False)
class GenPng(View):
    def handle_request(self, request: Request) -> None | str | AbstractNode | dict:
        data = ipc.pop(request.GET.get("id"))
        txt = data["txt"]
        req_formatter = data["formatter"]
        if not req_formatter or req_formatter not in formatter:
            return
        pdf = formatter[req_formatter]()
        pdf.add_text(txt)
        with tempfile.TemporaryDirectory() as td:
            td = pathlib.Path(td)
            pdff = td / "out.pdf"
            pngf = td / "out.png"
            pdf.output(pdff)
            subprocess.check_call(f"convert {pdff} {pngf}".split())
            with open(pngf, "rb") as fh:
                png = fh.read()
            return {
                "content_type": "image/x-png",
                "body": png,
            }


@app.route("/")
class Main(View):
    re_printer = re.compile(r"^printer (\S+) (.+) since")

    def _update_img(self, _event):
        req_id = lona.unique_ids.generate_unique_id2()
        d = {"id": req_id}
        ipc[req_id] = {"txt": self.ta_text.value, "formatter": self._sel_formatter}
        self.img_label.attributes["src"] = f"/genpng?{urlencode(d)}"
        self.div_error.set_text("")

    def _formatter_select(self, event):
        self._sel_formatter = event.node.formatter
        for btn in self.c_formatter:
            btn.outline = True
        event.node.outline = False
        self.ta_text.attributes["placeholder"] = getattr(formatter[self._sel_formatter], "HINT", "")
        self._update_img(None)

    def _printer_select(self, event):
        self._update_formatters(event.node.printer)
        for btn in self.c_printer:
            btn.outline = True
        event.node.outline = False
        self._update_img(None)

    def _update_formatters(self, printer):
        self.c_formatter.clear()
        for fmt in printer["formatter"]:
            self.c_formatter.append(
                btn := Button(
                    Icon(name="file-text", stroke_width=2),
                    "&nbsp",
                    formatter[fmt].NAME,
                    outline=True,
                    style="width: 300px; display: initial; margin-left: 10px;",
                    handle_click=self._formatter_select,
                )
            )
            btn.formatter = fmt
            btn.printer = printer
        self.c_formatter[0].outline = False
        self._sel_printer = printer
        self._sel_formatter = printer["formatter"][0]
        self.ta_text.attributes["placeholder"] = getattr(formatter[self._sel_formatter], "HINT", "Enter Text here...")

    @staticmethod
    def _select_printer_by_regex(regex):
        search = re.compile(regex)
        candidates = subprocess.check_output("lpstat -p", shell=True).decode().splitlines()
        for c in candidates:
            m = Main.re_printer.match(c)
            if m:
                if "disabled" not in m[2]:
                    if search.match(m[1]):
                        return m[1]
        return None

    def _print(self, event):
        self.btn_print.disabled = True
        self.btn_print.set_text("Printing... Please wait...")
        self.show()
        start = time.time()
        pdf = formatter[self._sel_formatter]()
        pdf.add_text(self.ta_text.value)
        with tempfile.TemporaryDirectory() as td:
            td = pathlib.Path(td)
            pdff = td / "out.pdf"
            pdf.output(pdff)
            printer = Main._select_printer_by_regex(self._sel_printer["printer_name"])
            if printer:
                # Print Label
                args = self._sel_printer.get("lpr_extra_args", "")
                subprocess.check_call(f"lpr -P {printer} {args} {pdff}", shell=True)
                self.div_error.set_text("")

                # Store History
                self.history.append_history(self.user, self.ta_text.value)
                self._update_history()

                now = time.time()
                self.sleep(max(0, start + 1 - now))
            else:
                self.div_error.set_text("Could not find printer!")

        self.btn_print.disabled = False
        self.btn_print.set_text("Print!")

    def _recall_history(self, event):
        self.ta_text.value = event.node.get_text()

    def _update_history(self):
        self.ul_history.nodes.clear()
        for h in self.history.get_history(self.user):
            self.ul_history.nodes.insert(0, Li(A(h, handle_click=self._recall_history, events=[CLICK])))

    def handle_request(self, request: Request) -> None | str | AbstractNode | dict:
        self.history = HistoryContainer()
        self.user = self.request.user.session_key

        html = HTML(
            H1("Label Generator"),
            c_printer := Card(),
            c_formatter := Card(),
            ta_text := TextArea(placeholder="Enter text here", handle_change=self._update_img),
            img_label := Img(src="", style="background: white;"),
            btn_print := Button("Print!", handle_click=self._print, style="margin: 10px"),
            div_err := Div(),
            ul_history := Ul(),
        )
        self.ta_text = ta_text
        self.img_label = img_label
        self.c_formatter = c_formatter
        self.c_printer = c_printer
        self.div_error = div_err
        self.ul_history = ul_history
        self.btn_print = btn_print

        for printer in config["printers"]:
            c_printer.append(
                btn := Button(
                    Icon(name="printer", stroke_width=2),
                    "&nbsp",
                    printer["label"],
                    outline=True,
                    style="width: 300px; display: initial; margin-left: 10px;",
                    handle_click=self._printer_select,
                )
            )
            btn.printer = printer
        c_printer[0].outline = False

        self._update_formatters(c_printer[0].printer)
        self._update_img(None)
        self._update_history()
        return html


app.run()

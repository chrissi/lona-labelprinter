# Lona PDF Gen

This tools allows you to print stuff directly from your browser.
It was developed with small label printers in mind - but could possibly be re-used for other stuff, too.

Under the hood we use FPDF2 to generate the pdfs.

## Systme Dependencies

Printing via CUPS must be set up. Make sure to install `cups-bsd` and not `lpr`! 
The `lpr`-command from the `lpr` package does not accept the CUPS printers as the `lpr`-command from `cups-bsd` 
does... (*Jeez! printing is cursed...*)

For the preview this tool converts PDFs to PNG using the `convert`-command from the `imagemagick` package.

## Python Dependencies

This tool is best used with a python `venv`:

``` shell
$ python -m venv venv
$ source venv/bin/activate
$ pip install -r requirements
```

## Running locally

Simply source the venv and run the `main.py`:
```shell
$ source venv/bin/activate
$ python main.py
```
By default the server will bind to localhost only.

Now open your webbrowser and surf to http://localhost:8080

## Running for the Internet

```shell
$ source venv/bin/activate
$ python main.py --host "*"
```
This will bind the server to every interface via IPv4 and IPv6. Bind to what ever you want :-)

## Configuration

The tool will need a `config.yaml`. This either searched in the directory of the `main.py` or a path can be
supplied via the `LABELPRINTER_CONFIG` environment variable.

`config.yaml.example` contains the most recent format with some comments on how to set this up.